<?php
/*This file contains the functions needed for diceGame.php  
		These functions will look at the playablilty of each option in a game of yahtzee
		and also will be able to add points to a user's hand.
*/
		
//Rolls a dice - generates a random number from 1 to 6.
function rollDice()
{   
	$rolledNum = rand(1,6);
	return $rolledNum;
}

//chooses which dice to roll
//@param $dice an array of prerolled dice
//@param d1-d5 value that if equal to 1, will not be rolled
//@param dALL value that if equal to 1 will not reroll any dice
function roll($dice, $d1, $d2, $d3, $d4, $d5, $dALL)
{
	if($dALL == 1)
	{    //keep all dice         
	}
	else
	{
		if($d1 != 1)
			$dice[0] = rollDice();
		if($d2 != 1)
			$dice[1] = rollDice();
		if($d3 != 1)
			$dice[2] = rollDice();
		if($d4 != 1)
			$dice[3] = rollDice();
		if($d5 != 1)
			$dice[4] = rollDice();
	}
	return $dice;
}
	
//returns the pictures of the dice that were rolled
#param $rolls  the array of the current dice rolls
function getDicePics($rolls)
{
		$rollsPictures = array(0,0,0,0,0);
		for($i = 0; $i < 5; $i++)
		{
		     if($rolls[$i] == 1)
			{	$rollsPictures[$i] = "<img src=\"dice1.jpg\"/>"; 	}
		else if($rolls[$i] == 2)
			{	$rollsPictures[$i] = "<img src=\"dice2.jpg\"/>";	}
		else if($rolls[$i] == 3)
			{	$rollsPictures[$i] = "<img src=\"dice3.jpg\"/>"; 	}
		else if($rolls[$i] == 4)
			{	$rollsPictures[$i] = "<img src=\"dice4.jpg\"/>"; 	}
		else if($rolls[$i] == 5)
			{	$rollsPictures[$i] = "<img src=\"dice5.jpg\"/>"; 	}
		else
			{	$rollsPictures[$i] = "<img src=\"dice6.jpg\"/>";	}		
		}
		return $rollsPictures;
}
	
//function for showing the score card
function showScoreCard()
{
	//gets the total of the top
	$iUpper = $_SESSION['ones'] + $_SESSION['twos'] + $_SESSION['threes'] + $_SESSION['fours'] + $_SESSION['fives'] + $_SESSION['sixes'];
		if($iUpper > 61)
		{
			$iUpperBonus = 35;
		}
		else
		{
			$iUpperBonus = 0;
		}		
	$iUpperTotal = $iUpper + $iUpperBonus;

	//total from the bottom
	$lowerScore = 	$_SESSION['threeKind'] + $_SESSION['fourKind'] + $_SESSION['fullHouse'] + $_SESSION['smStr'] + $_SESSION['lgStr'] + $_SESSION['yz'] + $_SESSION['chance'];

	$totalScore = $iUpperTotal + $lowerScore;
	//printing out the scorecard
		//Top part of a score card
		echo "Ones:\t" . $_SESSION['ones'] . "</br>";
		echo "Twos:\t" . $_SESSION['twos'] . "</br>";
		echo "Threes:\t" . $_SESSION['threes'] . "</br>";
		echo "Fours:\t" . $_SESSION['fours'] . "</br>";
		echo "Fives:\t" . $_SESSION['fives'] . "</br>";
		echo "Sixes:\t" . $_SESSION['sixes'] . "</br>";
		echo "Bonus:\t" . $iUpperBonus . "</br>";
		echo "Top Total: " . "\t" . $iUpperTotal . "</br>";
		echo "------------------" . "</br>";
		//Bottom Part of the score board
		echo "Three of a Kind:\t" . $_SESSION['threeKind'] . "</br>"; 	
		echo "Four of a Kind:\t" . $_SESSION['fourKind'] . "</br>";		
		echo "Full House:\t" . $_SESSION['fullHouse'] . "</br>";			
		echo "Small Straight:\t" . $_SESSION['smStr'] . "</br>";	
		echo "Large Straight:\t" . $_SESSION['lgStr'] . "</br>";	
		echo "YAHTZEE:\t" . $_SESSION['yz'] . "</br>";
		echo "Chance: " . "\t" . $_SESSION['chance'] . "</br>";
		echo "Bottom Total: " . "\t" . $lowerScore . "</br>";
		echo "------------------" . "</br>";
		//Grand Total
		?>
		<h3>Grand Total: <?=$_SESSION['score'];?></h3><?
}

//Function for testing what the user can play.
#@param array an array of the dice one has
function whatIsPlayable($array)
{
	
	?><h4>Scores for Current Roll</h4><?
	echo "Ones." . "\t" . "\t" . "Points: " . ones($array) . "</br>";
	echo "Twos." . "\t" . "\t" . "Points: " . twos($array) . "</br>";
	echo "Threes." . "\t" . "\t" . "Points: " . threes($array) . "</br>";
	echo "Fours." . "\t" . "\t" . "Points: " . fours($array) . "</br>";
	echo "Fives." . "\t" . "\t" . "Points: " . fives($array) . "</br>";
	echo "Sixes." . "\t" . "\t" . "Points: " . sixes($array) . "</br>";
	echo "</br>";
	echo "Three of a Kind." . "\t" . "Points: " . threeOfKind($array) . "</br>";
	echo "Four of a Kind." . "\t" . "Points: " . fourOfKind($array) . "</br>";
	echo "Full House." . "\t" . "Points: " . fullHouse($array) . "</br>";
	echo "Small Straight." . "\t" . "Points: " . smallStraight($array) . "</br>";		
	echo "Large Straight." . "\t" . "Points: " . largeStraight($array) . "</br>";		
	echo "YAHTZEE." . "\t" . "Points: " . yahtzee($array) . "</br>";		
	echo "Chance." . "\t" . "Points: " . chance($array) . "</br>";
}

##########################################################
##  These functions will be used for determining score  ##
//***FUNCTIONS FOR CALCULATING ONES-SIXES***
function ones($array)
{
$score = 0;
	for ($i = 0; $i < 5; $i++)
		if($array[$i] == 1)
		{	$score++;	}
			
return $score;
}

function twos($array)
{
$score = 0;
	for ($i = 0; $i < 5; $i++)
		if($array[$i] == 2)
		{	$score = $score + 2;	}
	
return $score;
}

function threes($array)
{
$score = 0;
	for ($i = 0; $i < 5; $i++)
		if($array[$i] == 3)
		{	$score = $score + 3;	}

return $score;
}

function fours($array)
{
$score = 0;
	for ($i = 0; $i < 5; $i++)
		if($array[$i] == 4)
		{	$score = $score + 4;	}
			
return $score;
}

function fives($array)
{
$score = 0;
	for ($i = 0; $i < 5; $i++)
		if($array[$i] == 5)
		{	$score = $score + 5;	}

return $score;
}

function sixes($array)
{
$score = 0;
	for ($i = 0; $i < 5; $i++)
		if($array[$i] == 6)
		{	$score = $score + 6;	}

return $score;
}


//--------------------------------
//***FUNCTIONS FOR isPlayable's

//checks if 3 of a kind is playable
function isPlayAbleThreeofKind($array)
{
sort($array);
$isValid = FALSE;
	for ($i = 0; $i < 3; $i++)
		if($array[$i] == $array[$i+1] && $array[$i] == $array[$i+2])
			$isValid = TRUE;

return $isValid;
}

//checks if 4 of a kind is playable
function isPlayableFourOfKind($array)
{
sort($array);
$isValid = FALSE;
	for ($i = 0; $i < 2; $i++)
		if($array[$i] == $array[$i + 1] && $array[$i] == $array[$i + 2] && $array[$i] == $array[$i + 3])
			$isValid = TRUE;

return $isValid;
}

//checks if the user is able to use hand as 4 of a kind
function isPlayableFullHouse($array)
{
sort($array);
$isValid = FALSE;
		if($array[0] == $array[1] && $array[0] == $array[2])
			{
				if($array[3] == $array[4])
				$isValid = TRUE;
			}

		if($array[0] == $array[1])
		{
			if($array[2] == $array[3] && $array[3] == $array[4])
				$isValid = TRUE;
		}				
return $isValid;
}


//calculates if the small straight is playable if the player has the correct requirements
function isPlayableSmallStraight($array)
{
sort($array);
$isValid = FALSE;
$num1 = 0;
$num2 = 0;
$num3 = 0;
$num4 = 0;
$num5 = 0;
$num6 = 0;

for($i = 0; $i < 5; $i++)
{
	if($array[$i] == 1)
	$num1++;
	else if($array[$i] == 2)
	$num2++;
	else if($array[$i] == 3)
	$num3++;
	else if($array[$i] == 4)
	$num4++;
	else if($array[$i] == 5)
	$num5++;
	else
	$num6++;
}

if(($num1 == (1 || 2)) && ($num2 == (1 || 2)) && ($num3 == (1 || 2)) && ($num4 == (1 || 2)))
		$isValid = TRUE;
if(($num2 == (1 || 2)) && ($num3 == (1 || 2)) && ($num4 == (1 || 2)) && ($num5 == (1 || 2)))
		$isValid = TRUE;
if(($num3 == (1 || 2)) && ($num4 == (1 || 2)) && ($num5 == (1 || 2)) && ($num6 == (1 || 2)))
		$isValid = TRUE;
		
return $isValid;
}

//checks if the user has a large straight
function isPlayableLargeStraight($array)
{
sort($array);
$isValid = FALSE;
	if($array[0] == (($array[1]) - 1) && $array[0] == (($array[2]) - 2) && $array[0] == (($array[3]) - 3) && $array[0] == (($array[4]) - 4))
		$isValid = TRUE;
		
return $isValid;
}

//checks if the user has yahtzee
function isPlayableYahtzee($array)
{
$isValid = FALSE;
	if($array[0] == $array[1] && $array[0] == $array[2] && $array[0] == $array[3] && $array[0] == $array[4])
		$isValid = TRUE;
return $isValid;
}

//***FUNCTIONS FOR ADDING SCORE

//returns the value of the three of a kind / 0 if not playable
function threeOfKind($array)
{
$score = 0;
	if(isPlayAbleThreeofKind($array) == TRUE)
		for($j = 0; $j < 5; $j++)
			$score += $array[$j];

return $score;
}

//returns the amount of points for 4 of a kind / 0 if is not playable
function fourOfKind($array)
{
$score = 0;
	if(isPlayableFourOfKind($array) == TRUE)
		for($j = 0; $j < 5; $j++)
			$score += $array[$j];
			
	return $score;
}

//returns 25 points if the user hand is playable / 0 if not playable
function fullHouse($array)
{
$score = 0;				
	if(isPlayableFullHouse($array) == TRUE)
		$score = 25;
		
return $score;
}

//returns 30 points if the user has a small straight / 0 if the user does not.
function smallStraight($array)
{
$score = 0;
	if(isPlayableSmallStraight($array) == TRUE)
		$score = 30;
return $score;
}

//returns 40 points if the user has a large straight / 0 if not.
function largeStraight($array)
{
$score = 0;
	if(isPlayableLargeStraight($array) == TRUE)
		$score = 40;
return $score;
}

//return 50 points if the user has a yahtzee / 0 points if the user does not
function yahtzee($array)
{
$score = 0;
	if(isPlayableYahtzee($array) == TRUE)
		$score = 50;
return $score;
}

//returns the amount of points in the users hand
function chance($array)
{
$score = 0;
	for ($i = 0; $i < 5; $i++)
		$score += $array[$i];
		
return $score;
}

//Function for adding points
function addPoints()
{
	$selectedOption = $_POST['choice'];
	if($selectedOption != "")	
	{	
		$_SESSION['choiceArray'][$selectedOption] = "";	
		//Gets the score depending on the last roll
		$prerolls[0] = $_SESSION["rolls"][0];
		$prerolls[1] = $_SESSION["rolls"][1];
		$prerolls[2] = $_SESSION["rolls"][2];
		$prerolls[3] = $_SESSION["rolls"][3];
		$prerolls[4] = $_SESSION["rolls"][4];
	
		$selectedOption = $_POST['choice'];
		if($selectedOption == 1)
		{	$_SESSION['score'] = $_SESSION['score'] + ones($prerolls);	
			$_SESSION['ones'] = ones($prerolls);	}
		if($selectedOption == 2)
		{	$_SESSION['score'] = $_SESSION['score'] + twos($prerolls);	
			$_SESSION['twos'] = twos($prerolls);	}
		if($selectedOption == 3)
		{	$_SESSION['score'] = $_SESSION['score'] + threes($prerolls);
			$_SESSION['threes'] = threes($prerolls);	}
		if($selectedOption == 4)
		{	$_SESSION['score'] = $_SESSION['score'] + fours($prerolls);	
			$_SESSION['fours'] = fours($prerolls);	}
		if($selectedOption == 5)
		{	$_SESSION['score'] = $_SESSION['score'] + fives($prerolls);	
			$_SESSION['fives'] = fives($prerolls);	}
		if($selectedOption == 6)
		{	$_SESSION['score'] = $_SESSION['score'] + sixes($prerolls);	
			$_SESSION['sixes'] = sixes($prerolls);	}
		if($selectedOption == 7)
		{	$_SESSION['score'] = $_SESSION['score'] + threeOfKind($prerolls);	
			$_SESSION['threeKind'] = threeOfKind($prerolls);	}
		if($selectedOption == 8)
		{	$_SESSION['score'] = $_SESSION['score'] + fourOfKind($prerolls);
			$_SESSION['fourKind'] = fourOfKind($prerolls);	}
		if($selectedOption == 9)
		{	$_SESSION['score'] = $_SESSION['score'] + fullHouse($prerolls);	
			$_SESSION['fullHouse'] = fullHouse($prerolls);	}
		if($selectedOption == 10)
		{	$_SESSION['score'] = $_SESSION['score'] + smallStraight($prerolls);
			$_SESSION['smStr'] = smallStraight($prerolls);	}
		if($selectedOption == 11)
		{	$_SESSION['score'] = $_SESSION['score'] + largeStraight($prerolls);	
			$_SESSION['lgStr'] = largeStraight($prerolls);	}
		if($selectedOption == 12)
		{	$_SESSION['score'] = $_SESSION['score'] + yahtzee($prerolls);	
			$_SESSION['yz'] = yahtzee($prerolls);	}
		if($selectedOption == 13)
		{	$_SESSION['score'] = $_SESSION['score'] + chance($prerolls);
			$_SESSION['chance'] = chance($prerolls);	}
	}
}