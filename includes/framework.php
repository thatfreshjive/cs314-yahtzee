<?php
namespace CLib;

/**
 * @property SQL $sql
 */
class CornellFramework extends Framework {
	public function __construct(){
		parent::__construct();
	}

	public function before_render() {
		$this->include_js('//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js');
		$this->include_js('app');
		$this->include_css('style');
		$this->set_template_section('body-class', 'header-dark');
	}

}