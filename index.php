<?php
namespace CLib;

require_once 'autoload.php';
require_once 'includes/config.php';
require_once 'includes/framework.php';

$fw = new CornellFramework();
$fw->init($config);
$fw->router();
$fw->render();