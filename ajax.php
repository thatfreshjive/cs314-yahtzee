<?php
namespace CLib;

//Setup core includes
require_once('autoload.php');
require_once('includes/config.php');
require_once('includes/framework.php');
header("Cache-Control: no-cache");

$fw = new CornellFramework();
$fw->init($config);
$fw->router();
$fw->render(true);
?>