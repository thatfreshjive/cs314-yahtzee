<html>
<head>
<?session_start();
require_once('W:\html\public\webteam\jsterchele17\dice-game\dice-game\includes\dice_game_functions.php');
?>
</head>
<title>Dice Game</title>
<?php
include('../../../../../assets/2014/includes/prototypes/std_header.inc');  
?>

<div class="body">  
    <section class="section l-two-col">
    <div class="wrapper">
<body>
<?php
	/** End Game **/
	if($_SESSION["round"] == 14)
	{
		?><META HTTP-EQUIV="refresh" CONTENT="0; 
		URL=http://www.cornellcollege.edu/webteam/jsterchele17/dice-game/dice-game/display/endGame.php">
		<?
	}

	/** playing the game **/
	else
	{
		//Div for the scoreCard
		?><div style="width: 200; height: 525; float:right; color: white; background-color:#4C1063; border:1px solid black;">
		Rounds: <?=$_SESSION["round"];?>  / 13 <br><br>
		<?=showScoreCard();
		?></div>
		<div style="width: 10px; height: 800px; float: right;">
			&nbsp;
		</div><?
	/*************1st Roll****************/
	if($_SESSION["timesRolled"] == 0)
	{
		?>
		<h2 align="center">	
		<p>Please Select The Dice You Want to Keep</p> 
		</h2>

		<?
		//ROLL
		for($i=0; $i<5; $i++)
			$_SESSION["rolls"][$i] = rollDice($_SESSION["rolls"][$i]);

		//DICE PICTURES
		$pics = getDicePics($_SESSION["rolls"]);
		?><div align="center">
		<? echo $pics[0] . $pics[1] . $pics[2] . $pics[3] . $pics[4];  
		   echo
		'<form method="post" action="game.php">
		  <input type="checkbox" value="1" name="d1"/>First Die  &emsp; &emsp;  
		  <input type="checkbox" value="1" name="d2"/>Second Die &emsp; &emsp;
		  <input type="checkbox" value="1" name="d3"/>Third Die  &emsp; &emsp;
		  <input type="checkbox" value="1" name="d4"/>Fourth Die &emsp; &emsp;
		  <input type="checkbox" value="1" name="d5"/>Fifth Die </br>
		  <input type="checkbox" value="1" name="dALL"/>Keep All the Dice'; 
		?>
		
		<p>You have rolled 1 time. <br> You have 2 rolls remaining.</p>

		<?echo	
		'<input type="submit" value="Re-roll Dice" name="rolldice">
		</form>';
		echo"</br>";
		whatIsPlayable($_SESSION["rolls"]);
		?></div><?

		//INCREMENTS TIMESROLLED 
		$_SESSION["timesRolled"] = 1;
		
	}


	/*************2nd Roll****************/
	elseif ($_SESSION["timesRolled"] == 1)
	{

		?><h2 align="center">	
		<p>Please Select The Dice You Want to Keep</p> 
		</h2><?

		//Function for rolling dice
		$_SESSION["rolls"] = roll($_SESSION["rolls"], $_POST["d1"], $_POST["d2"], $_POST["d3"], $_POST["d4"], $_POST["d5"], $_POST["dALL"]);

		//DICE PICTURES
		$pics = getDicePics($_SESSION["rolls"]);
		?><div align="center">
		<? echo $pics[0] . $pics[1] . $pics[2] . $pics[3] . $pics[4]; 
		   echo
				'<form method="post" action="game.php">
		  <input type="checkbox" value="1" name="d1"   <?php if(isset($_POST["d1"])) checked   ?>First Die  &emsp; &emsp;  
		  <input type="checkbox" value="1" name="d2"   <?php if(isset($_POST["d2"])) checked   ?>Second Die &emsp; &emsp;
		  <input type="checkbox" value="1" name="d3"   <?php if(isset($_POST["d3"])) checked   ?>Third Die  &emsp; &emsp;
		  <input type="checkbox" value="1" name="d4"   <?php if(isset($_POST["d4"])) checked   ?>Fourth Die &emsp; &emsp;
		  <input type="checkbox" value="1" name="d5"   <?php if(isset($_POST["d5"])) checked   ?>Fifth Die </br>
		  <input type="checkbox" value="1" name="dALL  <?php if(isset($_POST["dALL"])) ?>Keep All The Dice'; 
		?>
		
		<p>You have rolled 2 times. <br> You have 1 roll remaining.</p> 
		
		<?echo	
		'<input type="submit" value="Re-roll Dice" name="rolldice">
		</form>';
		echo"</br>";
		whatIsPlayable($_SESSION["rolls"]);
		?></div><?
		//INCREMENTS TIMESROLLED
		$_SESSION["timesRolled"] = 2;
	}

	elseif ($_SESSION["timesRolled"] == 2)
	{
		?><h2 align="center">Allocate your Points</h2><?

		//Function for rolling dice
		$_SESSION["rolls"] = roll($_SESSION["rolls"], $_POST["d1"], $_POST["d2"], $_POST["d3"], $_POST["d4"], $_POST["d5"], $_POST["dALL"]);
		//DICE PICTURES
		$pics = getDicePics($_SESSION["rolls"]);
		?><div align="center"><? 
		echo $pics[0] . $pics[1] . $pics[2] . $pics[3] . $pics[4]; 
		echo "</br>";
		whatIsPlayable($_SESSION["rolls"]);
		?></div><?

		/**********************************************************
	THIS IS WHERE THE USER MUST SELECT HOW HE/SHE ALLOCATES HIS/HER POINTS */
			$sMyOptions = "";
			for($j=1; $j<15; $j++)
			{	
				if($_SESSION['choiceArray'][$j] == "") 
					continue;
	
				if($_SESSION[$sSessionVariableName.$j] != 0)
				{ 
					if($j>1)
					{ $sMyOptions .= ', '; }
				}
				$sMyOptions .= '<option value="'. $j .'">';
				
				switch ($j) 
				{
					case 1:
					$myOptions .= "<option value=1>Select to use as your Ones</option>";
					break;
					
					case 2:
					$myOptions .= "<option value=2>Select to use as your Twos</option>";
					break;
					
					case 3:
					$myOptions .= "<option value=3>Select to use as your Threes</option>";
					break;
					
					case 4:
					$myOptions .= "<option value=4>select to use as your Fours</option>";
					break;
					
					case 5:
					$myOptions .= "<option value=5>Select to use as your Fives</option>";
					break;
					
					case 6:
					$myOptions .= "<option value=6>Select to use as your Sixes</option>";
					break;
					
					case 7:
					$myOptions .= "<option value=7>Select to use as your Three of a Kind</option>";
					break;
					
					case 8:
					$myOptions .= "<option value=8>Select to use as your Four of a Kind</option>";
					break;
					
					case 9:
					$myOptions .= "<option value=9>Select to use as your Full House</option>";
					break;
					
					case 10:
					$myOptions .= "<option value=10>Select to use as your Small Straight</option>";
					break;
					
					case 11:
					$myOptions .= "<option value=11>Select to use as your Large Straight</option>";
					break;
					
					case 12:
					$myOptions .= "<option value=12>Select to use as your Yahtzee</option>";
					break;
					
					case 13:
					$myOptions .= "<option value=13>Select to use as your Chance</option>";
					break;
				}
			}
			//prints out the options
			echo '<form method="post" action="game.php">';
			?><div align="center">
			<select name="choice">
				<?=$myOptions?>
			</select>
			</div>
			<div align="center">
			<? echo'<input type="submit">
			</form>';
			?></div><?
			$_SESSION["timesRolled"]++;
	}
	else
	{
		addPoints();
		//IRESETS TIMESROLLED
			$_SESSION["timesRolled"] = 0;
			$_SESSION["round"]++;
		?><meta http-equiv="refresh" content="0"><?
	}
}
?>
	</div>
	</META></section>
	<?include('../../../../../assets/2014/includes/prototypes/std_footer.inc');?>
	</html>
